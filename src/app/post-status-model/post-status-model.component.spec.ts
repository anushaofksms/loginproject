import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PostStatusModelComponent } from './post-status-model.component';

describe('PostStatusModelComponent', () => {
  let component: PostStatusModelComponent;
  let fixture: ComponentFixture<PostStatusModelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PostStatusModelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PostStatusModelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
