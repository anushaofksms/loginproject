import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RecognizationWallComponent } from './recognization-wall.component';

describe('RecognizationWallComponent', () => {
  let component: RecognizationWallComponent;
  let fixture: ComponentFixture<RecognizationWallComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RecognizationWallComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RecognizationWallComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
