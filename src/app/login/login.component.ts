import { Component, OnInit } from '@angular/core';
import { ListService } from './../list.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit { 

  constructor(private listService:ListService,
              private router:Router){}

  isFormSubmit: boolean = false;
  loginData:any;

  ngOnInit() {
  }

  login(loginData) {

    this.isFormSubmit = true;

    let data = {
        'email': 'eve.holt@reqres.in',
        'password': 'cityslicka'
    }
    this.listService.getLoggin(data).subscribe(response =>{
      this.loginData = response
      this.router.navigate(['/dashboard/list/myfeed']);
    }, error => {
      console.log('==Get error Details==', error)
    })
  }


  }
