import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PostNavModelComponent } from './post-nav-model.component';

describe('PostNavModelComponent', () => {
  let component: PostNavModelComponent;
  let fixture: ComponentFixture<PostNavModelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PostNavModelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PostNavModelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
