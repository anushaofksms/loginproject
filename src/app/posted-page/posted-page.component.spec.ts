import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PostedPageComponent } from './posted-page.component';

describe('PostedPageComponent', () => {
  let component: PostedPageComponent;
  let fixture: ComponentFixture<PostedPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PostedPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PostedPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
