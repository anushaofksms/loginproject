import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { TopNavBarComponent } from './top-nav-bar/top-nav-bar.component';
import { FacebookComponent } from './facebook/facebook.component';
import { BannerComponent } from './banner/banner.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { ProfilePictureComponent } from './profile-picture/profile-picture.component';
import { PostComponent } from './post/post.component';
import { ToggleButtonComponent } from './toggle-button/toggle-button.component';
import { RoundtabButtonComponent } from './roundtab-button/roundtab-button.component';
import { PostedPageComponent } from './posted-page/posted-page.component';
import { NotificationComponent } from './notification/notification.component';
import { RecognitionsComponent } from './recognitions/recognitions.component';
import { EventsComponent } from './events/events.component';
import { JobPostComponent } from './job-post/job-post.component';
import { ReferalComponent } from './referal/referal.component';
import { MyFeedComponent } from './my-feed/my-feed.component';
import { RecognizationWallComponent } from './recognization-wall/recognization-wall.component';
import { TagsComponent } from './tags/tags.component';
import { AllpostComponent } from './allpost/allpost.component';
import { ModalModule } from 'ngx-bootstrap/modal';
import { PostStatusModelComponent } from './post-status-model/post-status-model.component';
import { CreatePollModelComponent } from './create-poll-model/create-poll-model.component';
import { CreateEventModelComponent } from './create-event-model/create-event-model.component';
import { PostNavModelComponent } from './post-nav-model/post-nav-model.component';
import { MainModelComponent } from './main-model/main-model.component';

const routes: Routes = [
  { path: '', redirectTo: '/login', pathMatch: 'full' },
  { path: 'login', component: LoginComponent },
  { path: 'dashboard/list', component: FacebookComponent, 
    children:[
      { path: 'myfeed', component: MyFeedComponent},
      { path: 'recognizationwall', component: RecognizationWallComponent},
      { path: 'tags', component:TagsComponent},
      { path: 'events', component: EventsComponent},
      { path: 'allpost', component: AllpostComponent},
      { path: '**',redirectTo: 'my-network'}
    ]
  }
  // { path: 'my-network', component: MyNetworkComponent},
  // { path: 'my-activity', component: MyActivityComponent },
  // { path: 'my-bookmarks', component:MyBookmarksComponent }
];


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    TopNavBarComponent,
    FacebookComponent,
    BannerComponent,
    SidebarComponent,
    ProfilePictureComponent,
    PostComponent,
    ToggleButtonComponent,
    RoundtabButtonComponent,
    PostedPageComponent,
    NotificationComponent,
    RecognitionsComponent,
    EventsComponent,
    JobPostComponent,
    ReferalComponent,
    MyFeedComponent,
    RecognizationWallComponent,
    TagsComponent,
    AllpostComponent,
    PostStatusModelComponent,
    CreatePollModelComponent,
    CreateEventModelComponent,
    PostNavModelComponent,
    MainModelComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    RouterModule.forRoot(routes),
    ModalModule.forRoot()
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
