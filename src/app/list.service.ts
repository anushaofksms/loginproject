import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ListService {

  constructor(private httpClient: HttpClient) { }
  getLoggin(data) {
    return this.httpClient.post('https://reqres.in/api/login', data);
  }

}
