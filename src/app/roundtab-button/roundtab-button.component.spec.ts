import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RoundtabButtonComponent } from './roundtab-button.component';

describe('RoundtabButtonComponent', () => {
  let component: RoundtabButtonComponent;
  let fixture: ComponentFixture<RoundtabButtonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RoundtabButtonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RoundtabButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
