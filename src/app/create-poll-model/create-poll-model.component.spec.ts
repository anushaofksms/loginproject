import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreatePollModelComponent } from './create-poll-model.component';

describe('CreatePollModelComponent', () => {
  let component: CreatePollModelComponent;
  let fixture: ComponentFixture<CreatePollModelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreatePollModelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreatePollModelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
