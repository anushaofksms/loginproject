import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-create-poll-model',
  templateUrl: './create-poll-model.component.html',
  styleUrls: ['./create-poll-model.component.css']
})
export class CreatePollModelComponent implements OnInit {
  
  clickedChoice:boolean = false;
  constructor() { }

  ngOnInit() {
  }

  selectChoices()
  {
    this.clickedChoice = true;
  }

}
