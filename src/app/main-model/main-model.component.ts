import { Component, OnInit,Input,EventEmitter, Output} from '@angular/core';

@Component({
  selector: 'app-main-model',
  templateUrl: './main-model.component.html',
  styleUrls: ['./main-model.component.css']
})
export class MainModelComponent implements OnInit{
  
  model:boolean = true;
  someProperty:string='status';
  postStatusModel:boolean = true;
  createPollModel:boolean = false;
  createEventModel:boolean = false;
  @Output() crossbutton = new EventEmitter();
  @Input() name;
  
  ngOnInit() {
  }
  
  hide()
  {
    this.model = false;
    console.log(this.model);
  }

  cliked(value)
  {
    this.someProperty = value;
    
    if(value === 'status')
    {
      this.postStatusModel = true;
      this.createPollModel = false;
      this.createEventModel = false;
    }

    if(value === 'poll')
    {
      this.postStatusModel = false;
      this.createPollModel = true;
      this.createEventModel = false;
    }
    
    if(value === 'event')
    {
      this.postStatusModel = false;
      this.createPollModel = false;
      this.createEventModel = true;
    }
  }

  closeModal()
  {
    this.crossbutton.emit();
  }
}
