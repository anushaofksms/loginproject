import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateEventModelComponent } from './create-event-model.component';

describe('CreateEventModelComponent', () => {
  let component: CreateEventModelComponent;
  let fixture: ComponentFixture<CreateEventModelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateEventModelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateEventModelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
